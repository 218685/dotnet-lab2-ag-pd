﻿using System;
using System.Threading;

namespace GameOfLife
{
    public class Punkt
    {
	    public bool CzyZyje { get; set; }
	
	    public Punkt(Random generator)
	    {
		    CzyZyje = generator.NextDouble() > 0.499;
	    }

	    public Punkt(bool czyZyje)
	    {
		    CzyZyje = czyZyje;
	    }

		public override string ToString()
	    {
		    return CzyZyje ? "#" : ".";
	    }
    }
}
