﻿using System;

namespace GameOfLife
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var plansza = new Plansza(20);

			do
			{
				Console.Clear();
				Console.Write(plansza);
				plansza.PrzeliczNastepnaRunde();
			} while (Console.ReadLine() != "q" && plansza.NieWszystkiPunktySaMartwe());

	        Console.Clear();
			Console.WriteLine(plansza);
	        Console.ReadKey();
        }
    }
}