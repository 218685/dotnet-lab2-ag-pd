﻿using System;
using System.Text;

namespace GameOfLife
{
	public class Plansza
	{
		private int wielkoscBoku;

		private Punkt[,] plansza, kopia;

		public Plansza(int wielkoscBoku)
		{
			this.wielkoscBoku = wielkoscBoku + 2;
			plansza = new Punkt[this.wielkoscBoku, this.wielkoscBoku];
			kopia = new Punkt[this.wielkoscBoku, this.wielkoscBoku];

			StworzPlanszeZMartwymiPunktamiNaokolo();
			PrzepiszPlanszeDoKopii();
		}
		private void StworzPlanszeZMartwymiPunktamiNaokolo()
		{
			for (int j = 0; j < this.wielkoscBoku; j++)
			{
				plansza[0, j] = new Punkt(false);
				plansza[this.wielkoscBoku - 1, j] = new Punkt(false);
				if (plansza[j, 0] == null)
					plansza[j, 0] = new Punkt(false);
				if (plansza[j, this.wielkoscBoku - 1] == null)
					plansza[j, this.wielkoscBoku - 1] = new Punkt(false);
			}

			var generator = new Random();
			for (int i = 1; i < this.wielkoscBoku - 1; i++)
				for (int j = 1; j < this.wielkoscBoku - 1; j++)
					plansza[i, j] = new Punkt(generator);
		}

		private void PrzepiszPlanszeDoKopii()
		{
			for (int i = 0; i < this.wielkoscBoku; i++)
				for (int j = 0; j < this.wielkoscBoku; j++)
					kopia[i, j] = plansza[i, j];
		}

		public void PrzeliczNastepnaRunde()
		{
			for (int i = 1; i < wielkoscBoku - 1; i++)
				for (int j = 1; j < wielkoscBoku - 1; j++)
					SprawdzCzyPunktPrzezyje(i, j);

			PrzepiszPlanszeDoKopii();
		}

		private void SprawdzCzyPunktPrzezyje(int i, int j)
		{
			bool zyjeTeraz = plansza[i, j].CzyZyje;
			int ileSasiadow = WyliczIleSasiadowMaPunkt(i, j);

			if (zyjeTeraz)
			{
				if (ileSasiadow < 2 || ileSasiadow > 3)
					UsmiercPunkt(i, j);
			}
			else
			{
				if (ileSasiadow == 3)
				{
					OzywPunkt(i, j);
				}
			}
		}

		private int WyliczIleSasiadowMaPunkt(int i, int j)
		{
			int ileSasiadow = 0;
			for (int x = i - 1; x < i + 2; x++)
				for (int y = j - 1; y < j + 2; y++)
					if (PunktZyjeTeraz(x, y))
						ileSasiadow++;
			if (PunktZyjeTeraz(i, j))
				ileSasiadow--;

			return ileSasiadow;
		}

		private bool PunktZyjeTeraz(int i, int j)
		{
			return kopia[i, j].CzyZyje;
		}

		private void OzywPunkt(int i, int j)
		{
			plansza[i, j].CzyZyje = true;
		}

		private void UsmiercPunkt(int i, int j)
		{
			plansza[i, j].CzyZyje = false;
		}

		public override string ToString()
		{
			StringBuilder toReturn = new StringBuilder();
			for (int i = 1; i < wielkoscBoku - 1; i++)
			{
				toReturn.Append("\n");
				for (int j = 1; j < wielkoscBoku - 1; j++)
					toReturn.Append(plansza[i, j]);
			}

			return toReturn.ToString();
		}

		public bool NieWszystkiPunktySaMartwe()
		{
			foreach (var pkt in plansza)
			{
				if (pkt.CzyZyje)
					return true;
			}

			return false;
		}
	}
}
